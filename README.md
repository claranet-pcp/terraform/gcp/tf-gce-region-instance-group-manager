# tf-gce-region-instance-group

### Not yet implemented in terraform, awaiting PR: [10907](https://github.com/hashicorp/terraform/pull/10907)

## variables
* `service` - service name which is going to be running on those instances, will
  be used for hostname, tagging etc.
* `region` - Region to create instances in. Default: europe-west1
* `instance_template` -
* `target_size` - Number of instances to create spread across the
  region. Default: 3

## Outputs
* `instance_group` -
* `link` -

## Usage example
```
module "sso-group-manager" {
  source = "git::ssh://git@gogs.bashton.net/Bashton/tf-gce-region-instance-group-manager.git"

  service           = "sso"
  instance_template = "${module.sso-instance-template.link-subnets}"
  region            = "europe-west1"
  target_size       = 3
}
```
