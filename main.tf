resource "google_compute_region_instance_group_manager" "instance_group_manager" {
  name               = "${var.service}"
  instance_template  = "${var.instance_template}"
  base_instance_name = "${var.service}"
  update_strategy    = "${var.update_strategy}"
  region             = "${var.region}"
  target_size        = "${var.target_size}"
}
