variable "service"           {}
variable "instance_template" {}

variable "target_size" {
  default = "3"
}

variable "region" {
  default = "europe-west1"
}

variable "update_strategy" {
  default = "RESTART"
}

